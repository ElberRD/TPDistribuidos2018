/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.personasasignaturas.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.inject.Inject;
import py.una.pol.asignaturas.model.Asignatura;
import py.una.pol.personas.model.Persona;
import py.una.pol.personasasignaturas.dao.PersonaAsignaturaDAO;
import py.una.pol.asociacion.model.Asociacion;
import py.una.pol.personasasignaturas.model.PersonasAsignaturas;

/**
 *
 * @author elber
 */
public class PersonasAsignaturasService {
    @Inject
    private Logger log;

    @Inject
    private PersonaAsignaturaDAO dao;

    public void crearAsociacionAsignatura(Asociacion p) throws Exception {
        log.info("Asociando Asignatura a persona con ci: ");
        try {
                PersonasAsignaturas pAsig = new PersonasAsignaturas();
                pAsig.setCedula(dao.obtenerCedula(p));
                pAsig.setId(dao.obtenerId(p));
        	dao.AsociarPersonaAsignatura(pAsig);
        }catch(Exception e) {
        	log.severe("ERROR al asociar asignatura: " + e.getLocalizedMessage());
        	throw e;
        }
        log.info("Asignatura asociada con éxito... ");
    }
    
    public List<Persona> obtenerPersonas(String nombre) {
        List<Persona> lista = new ArrayList<Persona>();
        List<Asignatura> asignaturas = new ArrayList<Asignatura>();
        lista = dao.obtenerPorAsignaturasPersonas(nombre);
        for (Persona persona : lista) {
            asignaturas = dao.obtenerPorPersonasAsignaturas(persona.getNombre(), persona.getApellido());
            persona.setAsignaturas(asignaturas);
        }
        return lista;
    }
    
    public List<Asignatura> obtenerPorAsociacion(String nombre,String apellido) {
    	return (List<Asignatura>) dao.obtenerPorPersonasAsignaturas(nombre,apellido);
    }
    
    public long borrarAsociacion(long cedula,long id) throws Exception{
    	return dao.borrarAsociacion(cedula,id);
    }   
}
