/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.personasasignaturas.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author elber
 */
@SuppressWarnings("serial")
@XmlRootElement
public class PersonasAsignaturas {
    Long id;
    Long cedula;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCedula() {
        return cedula;
    }

    public void setCedula(Long cedula) {
        this.cedula = cedula;
    }
    
}
