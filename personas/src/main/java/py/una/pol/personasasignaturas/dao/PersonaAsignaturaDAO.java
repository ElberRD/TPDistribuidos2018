/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.personasasignaturas.dao;

/**
 *
 * @author elber
 */
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import py.una.pol.personasasignaturas.model.PersonasAsignaturas;
import javax.ejb.Stateless;
import javax.inject.Inject;
import py.una.pol.asignaturas.model.Asignatura;
import py.una.pol.personas.dao.Bd;
import py.una.pol.personas.model.Persona;
import py.una.pol.asociacion.model.Asociacion;

/**
 *
 * @author elber
 */
@Stateless
public class PersonaAsignaturaDAO {

    @Inject
    private Logger log;

    public List<Persona> obtenerPorAsignaturasPersonas(String nombre) {
        String SQL = "SELECT  persona.cedula, persona.nombre, persona.apellido "
                + "FROM asignatura INNER JOIN persona_asignatura "
                + "ON asignatura.id = persona_asignatura.id "
                + "INNER JOIN persona "
                + "ON persona_asignatura.cedula = persona.cedula where asignatura.nombre = ?";

        Persona p = null;
        List<Persona> lista = new ArrayList<Persona>();
        Connection conn = null;
        try {
            conn = Bd.connect();
            PreparedStatement pstmt = conn.prepareStatement(SQL);
            pstmt.setString(1, nombre);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                p = new Persona();
                p.setCedula(rs.getLong(1));
                p.setNombre(rs.getString(2));
                p.setApellido(rs.getString(3));
                lista.add(p);
            }

        } catch (SQLException ex) {
            log.severe("Error en obtener por asignaturas las personas: " + ex.getMessage());
        } finally {
            try {
                conn.close();
            } catch (Exception ef) {
                log.severe("No se pudo cerrar la conexion a BD: " + ef.getMessage());
            }
        }
        return lista;

    }

    public List<Asignatura> obtenerPorPersonasAsignaturas(String nombre, String apellido) {
        String SQL = "SELECT asignatura.id, asignatura.nombre "
                + "FROM persona INNER JOIN persona_asignatura "
                + "ON persona.cedula = persona_asignatura.cedula "
                + "INNER JOIN asignatura "
                + "ON persona_asignatura.id = asignatura.id where persona.nombre = ? and persona.apellido = ?";

        Asignatura p = null;
        List<Asignatura> lista = new ArrayList<Asignatura>();
        Connection conn = null;
        try {
            conn = Bd.connect();
            PreparedStatement pstmt = conn.prepareStatement(SQL);
            pstmt.setString(1, nombre);
            pstmt.setString(2, apellido);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                p = new Asignatura();
                p.setId(rs.getLong(1));
                p.setNombre(rs.getString(2));
                lista.add(p);
            }

        } catch (SQLException ex) {
            log.severe("Error en obtenerPorPersonas las Asignaturas: " + ex.getMessage());
        } finally {
            try {
                conn.close();
            } catch (Exception ef) {
                log.severe("No se pudo cerrar la conexion a BD: " + ef.getMessage());
            }
        }
        return lista;

    }

    //insercion de registro por ci y id
    public long AsociarPersonaAsignatura(PersonasAsignaturas p) throws SQLException {

        String SQL = "INSERT INTO persona_asignatura(cedula, id) "
                + "VALUES (?, ?)";

        long id = 0;
        Connection conn = null;

        try {
            conn = Bd.connect();
            PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            pstmt.setLong(1, p.getCedula());
            pstmt.setLong(2, p.getId());
            int affectedRows = pstmt.executeUpdate();
            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            }
        } catch (SQLException ex) {
            throw ex;
        } finally {
            try {
                conn.close();
            } catch (Exception ef) {
                log.severe("No se pudo cerrar la conexion a BD: " + ef.getMessage());
            }
        }

        return id;

    }

    //desasociar materia de personas
    public long borrarAsociacion(long cedula, long idAsig) throws SQLException {

        String SQL = "DELETE FROM persona_asignatura WHERE cedula = ? and id = ? ";
        long id = 0;
        Connection conn = null;
        try {
            conn = Bd.connect();
            PreparedStatement pstmt = conn.prepareStatement(SQL);
            pstmt.setLong(1, cedula);
            pstmt.setLong(2, idAsig);
            int affectedRows = pstmt.executeUpdate();
            // check the affected rows 
            if (affectedRows > 0) {
                id = idAsig;
            }

        } catch (SQLException ex) {
            log.severe("Error en la eliminación: " + ex.getMessage());
            throw ex;
        } finally {
            try {
                conn.close();
            } catch (Exception ef) {
                log.severe("No se pudo cerrar la conexion a BD: " + ef.getMessage());
                throw ef;
            }
        }
        return id;
    }

    public Long obtenerId(Asociacion p) throws SQLException {
        String SQL = "SELECT  id "
                + "FROM asignatura where nombre = ?";

        Long id = null;
        Connection conn = null;
        try {
            conn = Bd.connect();
            PreparedStatement pstmt = conn.prepareStatement(SQL);
            pstmt.setString(1, p.getAsignatura());
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                id = rs.getLong(1);
            }
        } catch (SQLException ex) {
            log.severe("Error en obtener cedula de la persona: " + ex.getMessage());
        } finally {
            try {
                conn.close();
            } catch (Exception ef) {
                log.severe("No se pudo cerrar la conexion a BD: " + ef.getMessage());
            }
        }
        return id;

    }

    public Long obtenerIdAsignatura(Asociacion p) throws SQLException {
        String SQL = "SELECT id "
                + "FROM asignatura where nombre = ? ";

        Long idAsig = null;
        Connection conn = null;
        try {
            conn = Bd.connect();
            PreparedStatement pstmt = conn.prepareStatement(SQL);
            pstmt.setString(1, p.getAsignatura());
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                idAsig = rs.getLong(1);
            }
        } catch (SQLException ex) {
            log.severe("Error en obtener cedula de la persona: " + ex.getMessage());
        } finally {
            try {
                conn.close();
            } catch (Exception ef) {
                log.severe("No se pudo cerrar la conexion a BD: " + ef.getMessage());
            }
        }
        return idAsig;

    }

    public Long obtenerCedula(Asociacion p) throws SQLException {
        String SQL = "SELECT  cedula "
                + "FROM persona where nombre = ? and apellido = ? ";

        Long cedula = null;
        Connection conn = null;
        try {
            conn = Bd.connect();
            PreparedStatement pstmt = conn.prepareStatement(SQL);
            pstmt.setString(1, p.getNombre());
            pstmt.setString(2, p.getApellido());
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                cedula = rs.getLong(1);
            }
        } catch (SQLException ex) {
            log.severe("Error en obtener cedula de la persona: " + ex.getMessage());
        } finally {
            try {
                conn.close();
            } catch (Exception ef) {
                log.severe("No se pudo cerrar la conexion a BD: " + ef.getMessage());
            }
        }
        return cedula;

    }
    
}
