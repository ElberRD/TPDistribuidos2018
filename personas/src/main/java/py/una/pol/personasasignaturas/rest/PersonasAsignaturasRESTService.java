/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.personasasignaturas.rest;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import py.una.pol.asignaturas.model.Asignatura;
import py.una.pol.personas.model.Persona;
import py.una.pol.asociacion.model.Asociacion;
import py.una.pol.personasasignaturas.dao.PersonaAsignaturaDAO;
import py.una.pol.personasasignaturas.model.PersonasAsignaturas;
import py.una.pol.personasasignaturas.service.PersonasAsignaturasService;

/**
 *
 * @author elber
 */
@Path("/personasasignaturas")
@RequestScoped
public class PersonasAsignaturasRESTService {

    @Inject
    private Logger log;

    @Inject
    PersonasAsignaturasService personaasignaturaService;

    @Inject 
    PersonaAsignaturaDAO dao;
    
    @GET
    @Path("/{nombre}/{apellido}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<Asignatura> listar(@PathParam("nombre") String nombre, @PathParam("apellido") String apellido) {
        return (List<Asignatura>) personaasignaturaService.obtenerPorAsociacion(nombre, apellido);
    }

    @GET
    @Path("/{nombre}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<Persona> listar(@PathParam("nombre") String nombre) {
        return (List<Persona>) personaasignaturaService.obtenerPersonas(nombre);
    }

    /**
     * Creates a new member from the values provided. Performs validation, and
     * will return a JAX-RS response with either 200 ok, or with a map of
     * fields, and related errors.
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(Asociacion p) {

        Response.ResponseBuilder builder = null;

        try {
            personaasignaturaService.crearAsociacionAsignatura(p);
            // Create an "ok" response

            //builder = Response.ok();
            builder = Response.status(201).entity("Asignatura asociada exitosamente");

        } catch (SQLException e) {
            // Handle the unique constrain violation
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("bd-error", e.getLocalizedMessage());
            builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }

        return builder.build();
    }

    @DELETE
    @Path("/{nombre}/{apellido}/{asignatura}")
    public Response borrar(@PathParam("nombre") String nombre,@PathParam("apellido") String apellido,@PathParam("asignatura") String asignatura) {
        Response.ResponseBuilder builder = null;
        try {
            Asociacion p = new Asociacion();
            p.setNombre(nombre);
            p.setApellido(apellido);
            p.setAsignatura(asignatura);
            if (personaasignaturaService.borrarAsociacion(dao.obtenerCedula(p),dao.obtenerIdAsignatura(p)) > 0) {
                builder = Response.status(202).entity("Asociacion borrada exitosamente.");
            } else {
                builder = Response.status(Response.Status.NOT_ACCEPTABLE).entity("Asociacion no existe.");
            }
        } catch (SQLException e) {
            // Handle the unique constrain violation
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("bd-error", e.getLocalizedMessage());
            builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }
        return builder.build();
    }

}