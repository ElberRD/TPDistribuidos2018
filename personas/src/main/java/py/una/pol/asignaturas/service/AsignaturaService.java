/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.asignaturas.service;

/**
 *
 * @author elber
 */
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;
import java.util.logging.Logger;
import py.una.pol.asignaturas.dao.AsignaturaDAO;
import py.una.pol.asignaturas.model.Asignatura;
import py.una.pol.personas.model.Persona;

// The @Stateless annotation eliminates the need for manual transaction demarcation
@Stateless
public class AsignaturaService {

    @Inject
    private Logger log;

    @Inject
    private AsignaturaDAO dao;

    public void crear(Asignatura p) throws Exception {
        log.info("Creando Asignatura: " + p.getNombre());
        try {
            dao.insertarAsignatura(p);
        } catch (Exception e) {
            log.severe("ERROR al crear asignatura: " + e.getLocalizedMessage());
            throw e;
        }
        log.info("Asignatura creada con éxito: " + p.getNombre());
    }

    public List<Asignatura> obtenerListaAsignaturas() {
        return dao.obtenerAsignaturas();
    }

    public void actualizarAsignaturas(Asignatura p) throws Exception {
        log.info("Modificando Asignatura: " + p.getNombre());
        try {
            dao.actualizar(p);
        } catch (Exception e) {
            log.severe("ERROR al crear asignatura: " + e.getLocalizedMessage());
            throw e;
        }
        log.info("Asignatura creada con éxito: " + p.getNombre());
    }

    public Asignatura obtenerPorId(long id) {
        return dao.obtenerPorId(id);
    }

    public long borrarAsignaturas(long id) throws Exception {
        return dao.borrarAsignatura(id);
    }
}
