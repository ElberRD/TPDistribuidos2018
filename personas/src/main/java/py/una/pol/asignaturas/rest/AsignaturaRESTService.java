/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.asignaturas.rest;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import py.una.pol.asignaturas.model.Asignatura;
import py.una.pol.asignaturas.service.AsignaturaService;

/**
 * JAX-RS Example
 * <p/>
 * Esta clase produce un servicio RESTful para leer y escribir contenido de
 * personas
 */
@Path("/asignaturas")
@RequestScoped
public class AsignaturaRESTService {

    @Inject
    private Logger log;

    @Inject
    AsignaturaService asignaturaService;

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<Asignatura> listar() {
        return asignaturaService.obtenerListaAsignaturas();
    }

    @GET
    @Path("/{id:[0-9]*}")
    @Produces(MediaType.APPLICATION_JSON)
    public Asignatura obtenerPorId(@PathParam("id") long id) {
        Asignatura p = asignaturaService.obtenerPorId(id);
        if (p == null) {
            log.info("obtenerPorId " + id + " no encontrado.");
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        log.info("obtenerPorId " + id + " encontrada: " + p.getNombre());
        return p;
    }

    @GET
    @Path("/id")
    @Produces(MediaType.APPLICATION_JSON)
    public Asignatura obtenerPorIdQuery(@QueryParam("id") long id) {
        Asignatura p = asignaturaService.obtenerPorId(id);
        if (p == null) {
            log.info("obtenerPorId " + id + " no encontrado.");
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        log.info("obtenerPorId " + id + " encontrada: " + p.getNombre());
        return p;
    }

    /**
     * Creates a new member from the values provided. Performs validation, and
     * will return a JAX-RS response with either 200 ok, or with a map of
     * fields, and related errors.
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(Asignatura p) {

        Response.ResponseBuilder builder = null;

        try {
            asignaturaService.crear(p);
            // Create an "ok" response

            //builder = Response.ok();
            builder = Response.status(201).entity("Asignatura creada exitosamente");

        } catch (SQLException e) {
            // Handle the unique constrain violation
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("bd-error", e.getLocalizedMessage());
            builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }

        return builder.build();
    }

    @POST
    @Path("/actualizar")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizar(Asignatura p) {

        Response.ResponseBuilder builder = null;

        try {
            asignaturaService.actualizarAsignaturas(p);
            // Create an "ok" response

            //builder = Response.ok();
            builder = Response.status(201).entity("Asignatura modificada exitosamente");

        }catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }
        // Handle the unique constrain violation

        return builder.build();
    }
    
    @DELETE
    @Path("/{id}")
    public Response borrar(@PathParam("id") long id) {
        Response.ResponseBuilder builder = null;
        try {
            if (asignaturaService.borrarAsignaturas(id) > 0) {
                builder = Response.status(202).entity("Asignatura borrada exitosamente.");
            } else {
                builder = Response.status(Response.Status.NOT_ACCEPTABLE).entity("Asignatura no existe.");
            }
        } catch (SQLException e) {
            // Handle the unique constrain violation
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("bd-error", e.getLocalizedMessage());
            builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }
        return builder.build();
    }

}
